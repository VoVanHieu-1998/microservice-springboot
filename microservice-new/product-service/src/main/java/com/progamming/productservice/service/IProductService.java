package com.progamming.productservice.service;

import com.progamming.productservice.dto.ProductRequest;
import com.progamming.productservice.dto.ProductResponse;

import java.util.List;

public interface IProductService {
    public void createProduct(ProductRequest productRequest);

    List<ProductResponse> getAllProduct();
}
