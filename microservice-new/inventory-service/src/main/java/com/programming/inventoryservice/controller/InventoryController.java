package com.programming.inventoryservice.controller;

import com.programming.inventoryservice.dto.InventoryResponse;
import com.programming.inventoryservice.service.IInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {
    @Autowired
    IInventoryService iInventoryService;
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isInStock(@RequestParam List<String> skuCode){
        return iInventoryService.isInStock(skuCode);
    }

    @GetMapping("/test/{skuCode}")
    @ResponseStatus(HttpStatus.OK)
    public boolean isInStockTest(@PathVariable String skuCode){
        return iInventoryService.isInStockTest(skuCode);
    }
}
