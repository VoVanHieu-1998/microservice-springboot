package com.programming.inventoryservice.service.impl;

import com.programming.inventoryservice.service.ITest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class Test1 implements ITest {
    @Override
    public void logData() {
        log.info("Test 1");
    }
}
