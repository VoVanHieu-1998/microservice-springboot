package com.programming.inventoryservice.service;

import com.programming.inventoryservice.dto.InventoryResponse;
import com.programming.inventoryservice.model.Inventory;

import java.util.List;

public interface IInventoryService {
    boolean isInStockTest(String skuCode);
    List<InventoryResponse> isInStock(List<String> skuCode);
}
