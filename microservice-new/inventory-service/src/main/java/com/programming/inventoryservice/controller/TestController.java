package com.programming.inventoryservice.controller;

import com.programming.inventoryservice.service.ITest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @Qualifier("test1")
    @Autowired
    ITest iTest;

}
