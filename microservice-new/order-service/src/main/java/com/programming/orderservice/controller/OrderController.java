package com.programming.orderservice.controller;

import com.programming.orderservice.dto.OrderLineItemsDto;
import com.programming.orderservice.dto.OrderRequest;
import com.programming.orderservice.dto.OrderResponse;
import com.programming.orderservice.model.Order;
import com.programming.orderservice.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    IOrderService orderService;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String placeOrder(@RequestBody OrderRequest orderRequest){
        boolean result = orderService.placeOrder(orderRequest);
        return result ? "Order Success" : "Order Fail. Product not in Stock";
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        OrderResponse order = orderService.findById(id);
        if(order == null){
            return ResponseEntity.badRequest().body("Order is null");
        }
        return ResponseEntity.status(HttpStatus.OK).body(order);
    }
    @GetMapping("/all-data")
    public ResponseEntity<?> getAllData(){
        List<OrderLineItemsDto> data = orderService.getAllData();
        if(data == null){
            return ResponseEntity.badRequest().body("Order is null");
        }
        return ResponseEntity.status(HttpStatus.OK).body(data);
    }
}
