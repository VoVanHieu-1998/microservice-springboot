package com.programming.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponse {
    private List<OrderLineItemsDto> orderLineItemsDtoList;
    private Long id;
    private String orderNumber;
}
