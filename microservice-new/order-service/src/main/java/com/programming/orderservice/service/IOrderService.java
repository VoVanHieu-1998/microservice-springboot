package com.programming.orderservice.service;

import com.programming.orderservice.dto.OrderLineItemsDto;
import com.programming.orderservice.dto.OrderRequest;
import com.programming.orderservice.dto.OrderResponse;

import java.util.List;

public interface IOrderService {
    boolean placeOrder(OrderRequest orderRequest);
    OrderResponse findById(Long id);
    List<OrderLineItemsDto> getAllData();
}
