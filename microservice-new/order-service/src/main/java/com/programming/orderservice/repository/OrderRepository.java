package com.programming.orderservice.repository;

import com.programming.orderservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findById(Long id);
    @Query("select o.id,o.orderNumber,ot.skuCode,ot.quantity,ot.price,ot.id as item_id from Order o inner join OrderLineItems ot ON o.id= ot.order.id")
    List<Object[]> getAllData();
    @Query(value = "select o.id,o.order_number,ot.sku_code,ot.quantity,ot.price,ot.id as item_id from orders o inner join order_line_items ot ON o.id= ot.order_id", nativeQuery = true)
    List<Object[]> getAllData2();
}
