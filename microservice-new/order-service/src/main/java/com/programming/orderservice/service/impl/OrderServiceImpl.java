package com.programming.orderservice.service.impl;

import com.programming.orderservice.dto.InventoryResponse;
import com.programming.orderservice.dto.OrderLineItemsDto;
import com.programming.orderservice.dto.OrderRequest;
import com.programming.orderservice.dto.OrderResponse;
import com.programming.orderservice.model.Order;
import com.programming.orderservice.model.OrderLineItems;
import com.programming.orderservice.repository.OrderRepository;
import com.programming.orderservice.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements IOrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    WebClient.Builder webClientBuilder;
    @Override
    public boolean placeOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());
        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtoList().stream()
                .map((OrderLineItemsDto orderLineItemsDto) -> mapToDto(orderLineItemsDto, order)).collect(Collectors.toList());
        order.setOrderLineItemsList(orderLineItems);

        List<String> skuCodes = order.getOrderLineItemsList().stream()
                .map(OrderLineItems::getSkuCode)
                .collect(Collectors.toList());
        //call stock
        InventoryResponse[] inventoryResponses = webClientBuilder.build().get()
                    .uri("http://inventory-service/api/inventory",
                            uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build())
                    .retrieve()
                    .bodyToMono(InventoryResponse[].class)
                    .block();
        boolean result = Arrays.stream(inventoryResponses).allMatch(InventoryResponse::isInStock);
        if(result){
            orderRepository.save(order);
            return true;
        }else{
            log.info("Product is not in Stock, please try again later.");
            return false;
        }
    }

    @Override
    public OrderResponse findById(Long id) {
        Optional<Order> optional = orderRepository.findById(id);
        if(optional.isPresent()){
            OrderResponse orderResponse = mapToOrderResponse(optional.get());
            return orderResponse;
        }
        return null;
    }

    @Override
    public List<OrderLineItemsDto> getAllData() {
        List<Object[]> data = orderRepository.getAllData2();
        List<OrderLineItemsDto> result = new ArrayList<>();
        if(data != null){
            for (Object[] item: data) {
                //o.id,o.orderNumber,ot.skuCode,ot.quantity,ot.price,ot.id as item_id
                OrderLineItemsDto orderLineItemsDto = new OrderLineItemsDto();
                orderLineItemsDto.setOrderId(item[0] != null ? Long.parseLong(item[0].toString()) : null);
                orderLineItemsDto.setOrderNumber(item[1] != null ? item[1].toString() : null);
                orderLineItemsDto.setSkuCode(item[2] != null ? item[2].toString() : null);
                orderLineItemsDto.setQuantity(item[3] != null ? Integer.parseInt(item[3].toString()) : null);
                orderLineItemsDto.setPrice(item[4] != null ? (BigDecimal) item[4] : null);
                orderLineItemsDto.setId(item[5] != null ? Long.parseLong(item[5].toString()) : null);
                result.add(orderLineItemsDto);
            }
        }
        return result;
    }

    private OrderLineItems mapToDto(OrderLineItemsDto orderLineItemsDto, Order order) {
        OrderLineItems orderLineItems = new OrderLineItems();
        orderLineItems.setPrice(orderLineItemsDto.getPrice());
        orderLineItems.setQuantity(orderLineItemsDto.getQuantity());
        orderLineItems.setSkuCode(orderLineItemsDto.getSkuCode());
        orderLineItems.setOrder(order);
        return orderLineItems;
    }
    private OrderResponse mapToOrderResponse(Order order) {
        OrderResponse response = new OrderResponse();
        response.setId(order.getId());
        response.setOrderNumber(order.getOrderNumber());
        if(order.getOrderLineItemsList() != null){
            response.setOrderLineItemsDtoList(order.getOrderLineItemsList().stream()
                    .map(item -> mapToResponse(item)).collect(Collectors.toList()));
        }
        return response;
    }
    private OrderLineItemsDto mapToResponse(OrderLineItems OrderLineItems) {
        OrderLineItemsDto orderLineItemsDto = new OrderLineItemsDto();
        orderLineItemsDto.setPrice(OrderLineItems.getPrice());
        orderLineItemsDto.setQuantity(OrderLineItems.getQuantity());
        orderLineItemsDto.setSkuCode(OrderLineItems.getSkuCode());
        orderLineItemsDto.setId(OrderLineItems.getId());
        return orderLineItemsDto;
    }
}
