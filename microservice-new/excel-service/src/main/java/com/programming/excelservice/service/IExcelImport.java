package com.programming.excelservice.service;

import com.programming.excelservice.model.excel.Item;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IExcelImport {
    public List<Item> importData(MultipartFile multipartFile) throws Exception;
}
