package com.programming.excelservice.service.imp;

import com.programming.excelservice.service.IExcelImport;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.programming.excelservice.model.excel.Item;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
@Service
public class ExcelImportImpl implements IExcelImport {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    @Override
    public List<Item> importData(MultipartFile file) throws Exception{
        List<Item> items = new ArrayList<>();
        try (InputStream inputStream = file.getInputStream()) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet worksheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = worksheet.iterator();
            rowIterator.next();

            while (rowIterator.hasNext()) {
                XSSFRow row = (XSSFRow) rowIterator.next();
                Item item = new Item();
                item.setName(row.getCell(0).getStringCellValue());
                item.setPrice(row.getCell(1).getNumericCellValue());
                item.setCreatedBy(LocalDateTime.parse(row.getCell(2).getStringCellValue(),formatter));

                items.add(item);
            }

            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error importing data from Excel file", e);
        }

        return items;
    }
}
