package com.programming.excelservice.controller;

import com.programming.excelservice.model.excel.Item;
import com.programming.excelservice.service.IExcelImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ExcelImportController {

    @Autowired
    private IExcelImport excelImport;

    @PostMapping("/import")
    public ResponseEntity<Object> importExcel(@RequestParam("file") MultipartFile file) {
        try {
            List<Item> importedItems = excelImport.importData(file);
            return ResponseEntity.ok(importedItems);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error: " + e.getMessage());
        }
    }
}

