package com.programming.emailservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateRequestDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String subject;
    private Location location;
    private BodyDTO body;
    private DatetimeDTO start;
    private DatetimeDTO end;
    private Organizer organizer;
    private List<Attendee> attendees;

}
