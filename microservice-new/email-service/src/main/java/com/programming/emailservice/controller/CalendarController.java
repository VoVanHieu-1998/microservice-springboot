package com.programming.emailservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.programming.emailservice.dto.CreateRequestDTO;
import com.programming.emailservice.exception.BadRequestException;
import com.programming.emailservice.response.ApiStatusCode;
import com.programming.emailservice.service.OutlookCalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin({"*"})
@RequestMapping({"/api/outlook"})
public class CalendarController extends ApplicationObjectSupport {
    @Autowired
    OutlookCalendarService outlookCalendarService;
    @GetMapping({"/test"})
    public String test() {
        return "test";
    }

    @PostMapping({"/createCalendar"})
    public ResponseEntity<Object> createOutlookCalendar(@RequestHeader("Authorization") String authorization,
                                                        @RequestBody CreateRequestDTO dto) throws BadRequestException{
        Map<String, Object> map = new HashMap<>();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String response = outlookCalendarService.createOutlookEvent(authorization,dto);
            map.put("status", ApiStatusCode.SUCCESS.getValue());
            map.put("message", ApiStatusCode.SUCCESS.getKey());
            map.put("result", objectMapper.readValue(response,Object.class));
            return ResponseEntity.status(HttpStatus.OK).body(map);
        }catch (Exception e){
            map.put("status", ApiStatusCode.BAD_REQUEST.getValue());
            map.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(map);
        }
    }
}
