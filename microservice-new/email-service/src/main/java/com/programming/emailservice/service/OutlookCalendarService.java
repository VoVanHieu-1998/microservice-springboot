package com.programming.emailservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.programming.emailservice.dto.CreateRequestDTO;
import com.programming.emailservice.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class OutlookCalendarService {
    @Value("${outlook.api.endpoint}")
    private String outlookApiEndpoint;

    @Value("${outlook.calendar.url}")
    private String outlookCalendarUrl;

    public String createOutlookEvent(String accessToken, CreateRequestDTO createRequestDTO) throws BadRequestException {
        WebClient webClient = WebClient.builder()
                .baseUrl(outlookApiEndpoint)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION,  accessToken)
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        String requestBodyJson;
        try {
            requestBodyJson = objectMapper.writeValueAsString(createRequestDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new BadRequestException("Missing inputs");
        }

        String response = webClient.post()
                .uri( "me/events")
                .body(BodyInserters.fromValue(requestBodyJson))
                .retrieve()
                .bodyToMono(String.class).block();
        return response != null ? response : "";
    }

}
