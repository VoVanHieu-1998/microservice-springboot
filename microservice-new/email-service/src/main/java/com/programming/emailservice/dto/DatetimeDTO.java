package com.programming.emailservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DatetimeDTO implements Serializable {
    private static final long serialVersionUID = 7537437707165991898L;
    private String dateTime;
    private String timeZone;

}
