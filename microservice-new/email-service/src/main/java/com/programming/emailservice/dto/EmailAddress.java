package com.programming.emailservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmailAddress implements Serializable {
    private static final long serialVersionUID = 1L;
    private String address;
    private String name;
}
